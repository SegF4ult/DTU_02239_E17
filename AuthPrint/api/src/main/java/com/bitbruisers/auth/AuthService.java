package com.bitbruisers.auth;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface AuthService extends Remote {
	AuthToken authenticate(String user, String password) throws AuthException, RemoteException;
}
