package com.bitbruisers.auth;

import java.util.Date;

public interface AuthToken {
	String getIssuer();
	String getSubject();
	int getUserId();
	Date getIssuedAt();
	Date getExpiresAt();
	String getJws();
}
