package com.bitbruisers.auth;

public class AuthTokenInvalidException extends Exception {
	public AuthTokenInvalidException() {
		super();
	}

	public AuthTokenInvalidException(String message) {
		super(message);
	}

	public AuthTokenInvalidException(Throwable cause) {
		super(cause);
	}

	public AuthTokenInvalidException(String message, Throwable cause) {
		super(message, cause);
	}
}
