package com.bitbruisers.auth;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import java.io.Serializable;
import java.util.Date;

public class AuthTokenImpl implements AuthToken, Serializable {
	private static final long serialVersionUID = 42L;
	private String jws = null;

	protected AuthTokenImpl() {}

	protected AuthTokenImpl(String jws) {
		this.jws = jws;
	}

	public String getIssuer() {
		return getTokenBody().getIssuer();
	}

	public int getUserId() {
		return (int)getTokenBody().get("uid");
	}

	public String getSubject() {
		return getTokenBody().getSubject();
	}

	public Date getIssuedAt() {
		return getTokenBody().getIssuedAt();
	}

	public Date getExpiresAt() {
		return getTokenBody().getExpiration();
	}

	public String getJws() {
		return this.jws;
	}

	protected void setJws(String jws) {
		this.jws = jws;
	}

	private Claims getTokenBody() {
		int i = this.jws.lastIndexOf('.');
		String jwt = this.jws.substring(0, i+1);
		return Jwts.parser()
				.parseClaimsJwt(jwt)
				.getBody();
	}
}
