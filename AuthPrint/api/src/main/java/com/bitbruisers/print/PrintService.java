package com.bitbruisers.print;

import com.bitbruisers.auth.AuthToken;
import com.bitbruisers.auth.AuthTokenInvalidException;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface PrintService extends Remote {
	
	public void print(AuthToken tkn, String filename, String printer) throws AuthTokenInvalidException, RemoteException;
	public void queue(AuthToken tkn) throws AuthTokenInvalidException, RemoteException;
	public void topQueue(AuthToken tkn, int job) throws AuthTokenInvalidException, RemoteException;
	public void start(AuthToken tkn) throws AuthTokenInvalidException, RemoteException;
	public void stop(AuthToken tkn) throws AuthTokenInvalidException, RemoteException;
	public void restart(AuthToken tkn) throws AuthTokenInvalidException, RemoteException;
	public void status(AuthToken tkn) throws AuthTokenInvalidException, RemoteException;
	public void readConfig(AuthToken tkn, String parameter) throws AuthTokenInvalidException, RemoteException;
	public void setConfig(AuthToken tkn, String parameter, String value) throws AuthTokenInvalidException, RemoteException;

}
