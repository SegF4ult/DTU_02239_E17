package com.bitbruisers;

import com.bitbruisers.auth.AuthException;
import com.bitbruisers.auth.AuthService;
import com.bitbruisers.auth.AuthToken;
import com.bitbruisers.auth.AuthTokenInvalidException;
import com.bitbruisers.print.PrintService;
import java.rmi.NotBoundException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.RemoteException;
import javax.rmi.ssl.SslRMIClientSocketFactory;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class PrintClientApp {
	private static final String RMI_HOST = "localhost";
	private static final int RMI_PORT = 42000;
	private static final Logger LOGGER = LogManager.getRootLogger();
	private static final String TRUSTSTORE_FILE = "client.truststore";
	private static final String TRUSTSTORE_PASS = "storepass";

	public static void main(String[] args) {
		LOGGER.info("Setting up application keystore...");
		System.setProperty("javax.net.ssl.trustStore", TRUSTSTORE_FILE);	
		System.setProperty("javax.net.ssl.trustStorePassword", TRUSTSTORE_PASS);

		LOGGER.info("Setting up secure socket factories...");
		SslRMIClientSocketFactory csf = new SslRMIClientSocketFactory();

		LOGGER.info("Initialization complete...");
		Registry rmi_reg = null;
		AuthService asvc = null;
		PrintService apsvc = null;

		AuthToken myToken = null;

		try {
			LOGGER.info("Accessing RMI registry at " + RMI_HOST + ":" + Integer.toString(RMI_PORT) + "...");
			rmi_reg = LocateRegistry.getRegistry(RMI_HOST,RMI_PORT, csf);

			LOGGER.info("Looking for authentication interface...");
			asvc = (AuthService) rmi_reg.lookup("auth");
			LOGGER.info("Looking for printing interface...");
			apsvc = (PrintService) rmi_reg.lookup("print");

		} catch(RemoteException re) {
			LOGGER.error("An exception has occurred: ");
			re.printStackTrace();
		} catch(NotBoundException nbe) {
			LOGGER.error("Service is not bound..");
			nbe.printStackTrace();
		}

		try {
			LOGGER.info("Authenticating with credentials (Alice, password)...");
			myToken = asvc.authenticate("Alice","password");
			LOGGER.info("myToken.getIssuer() = " + myToken.getIssuer());
			LOGGER.info("myToken.getSubject() = " + myToken.getSubject());
			LOGGER.info("myToken.getUserId() = " + Integer.toString(myToken.getUserId()));
			LOGGER.info("myToken.getIssuedAt() = " + myToken.getIssuedAt());
			LOGGER.info("myToken.getExpiresAt() = " + myToken.getExpiresAt());
		} catch(AuthException aex) {
			myToken = null;
			LOGGER.warn("(David, password) is an invalid combination");
		} catch(RemoteException rex) {
			LOGGER.error("An exception has occurred: ");
			rex.printStackTrace();			
		}

		try {
			apsvc.print(myToken, "Client.java","HP Deskjet");
			apsvc.queue(myToken);
			apsvc.topQueue(myToken, 0);
			apsvc.start(myToken);
			apsvc.stop(myToken);
			apsvc.restart(myToken);
			apsvc.status(myToken);
			apsvc.readConfig(myToken, "letter-size");
			apsvc.setConfig(myToken, "color", "false");
		} catch(RemoteException re) {
			LOGGER.error(re.getMessage());
		} catch(AuthTokenInvalidException atie) {
			LOGGER.error(atie.getMessage());
		}
	}
}
