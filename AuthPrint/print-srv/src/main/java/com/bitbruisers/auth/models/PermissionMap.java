package com.bitbruisers.auth.models;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.HashMap;
import java.util.Map;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import java.io.IOException;

public class PermissionMap {
	private static final Logger LOGGER = LogManager.getRootLogger();
	private Map<String, Map<String, Boolean>> permissions;
	public PermissionMap() {
		super();
		this.permissions = new HashMap<String, Map<String, Boolean>>();
	}

	public PermissionMap(String json) {
		super();
		
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Map<String, Boolean>> permMap = null;
		try {
			permMap = mapper.readValue(json, new TypeReference<Map<String, Map<String, Boolean>>>(){});
			if (permMap != null) {
				this.permissions = permMap;
			}
		} catch(JsonProcessingException jpe) {
			LOGGER.error(jpe.getMessage());
		} catch(IOException ioe) {
			LOGGER.error(ioe.getMessage());
		}
	}

	public boolean hasPermission(String permissionGroup, String permission) {
		Map<String, Boolean> map = this.permissions.get(permissionGroup);
		if (map == null) {
			return false;
		}

		if (!map.containsKey(permission))
			return false;

		return map.get(permission);
	}

	public boolean hasGroup(String permissionGroup) {
		return this.permissions.containsKey(permissionGroup);	
	}

	public void merge(PermissionMap rhs) {
		for(String group : rhs.permissions.keySet()) {
			this.permissions.merge(group, rhs.permissions.get(group), (m1, m2) -> {m1.putAll(m2);return m1;});
		}
	}

	@Override
	public String toString() {
		return this.permissions.toString();
	}
}
