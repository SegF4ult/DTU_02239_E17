package com.bitbruisers.auth;

import com.bitbruisers.auth.models.PermissionMap;
import com.bitbruisers.auth.models.Role;
import com.bitbruisers.auth.models.User;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Base64;
import java.util.HashMap;
import java.util.Stack;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.mindrot.jbcrypt.BCrypt;

public class AuthDatabase {
/*
 * Required instances
 */
	private static final Logger LOGGER = LogManager.getRootLogger();
	private static final AuthDatabase instance = new AuthDatabase();
	private Connection dbConn = null;
/*
 * Connection constants
 */
	private static final String DB_URL = "jdbc:sqlite:auth.db";

/*
 * SQL Table/View Create statements
 */
	private static final String SQL_TBL_CHECK = "SELECT count(*) FROM sqlite_master "
		   										+"WHERE type='table' "
												+"AND name=?";
	private static final String SQL_TBL_CONFIG = "CREATE TABLE `tbl_config` (\n"
												+"`config_id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\n"
												+"`config_key` TEXT NOT NULL UNIQUE,\n"
												+"`config_value` TEXT NOT NULL\n"
												+");";
	private static final String SQL_TBL_USERS = "CREATE TABLE `tbl_users` (\n"
												+"`user_id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\n"
												+"`user_name` TEXT NOT NULL UNIQUE,\n"
												+"`user_passwd_hash` TEXT NOT NULL\n"
												+");";
	private static final String SQL_TBL_USER_PERMISSIONS = "CREATE TABLE `tbl_user_permissions` (\n"
												+"`user_id`	INTEGER NOT NULL UNIQUE,\n"
												+"`user_permissions` TEXT,\n"
												+"FOREIGN KEY(`user_id`) REFERENCES tbl_users(user_id)\n"
												+")";
	private static final String SQL_TBL_ROLES = "CREATE TABLE `tbl_roles` (\n"
												+"`role_id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, \n"
												+"`role_name` TEXT NOT NULL UNIQUE,\n"
												+"`role_permissions` TEXT,\n"
												+"`role_inherits_from` INTEGER,\n"
												+"FOREIGN KEY(`role_inherits_from`) REFERENCES tbl_roles(role_id)\n"
												+")";
	private static final String SQL_TBL_USER_ROLES = "CREATE TABLE `tbl_user_roles` (\n"
												+"`user_id` INTEGER NOT NULL, \n"
												+"`role_id` INTEGER NOT NULL, \n"
												+"FOREIGN KEY(`user_id`) REFERENCES tbl_users(user_id),\n"
												+"FOREIGN KEY(`role_id`) REFERENCES tbl_roles(role_id)\n"
												+")";
	private static final String SQL_VIEW_USER_PERMISSIONS = "CREATE VIEW view_user_permissions (user_id, user_name, user_passwd_hash, user_permissions) AS\n"
												+"SELECT u.*, up.user_permissions FROM tbl_users AS u INNER JOIN tbl_user_permissions AS up ON up.user_id = u.user_id;";
	private static final String SQL_VIEW_USER_ROLES = "CREATE VIEW view_user_roles (user_id, user_name, user_passwd_hash, user_permissions, role_id) AS\n"
												+"SELECT vup.*, ur.role_id FROM view_user_permissions AS vup INNER JOIN tbl_user_roles AS ur ON ur.user_id = vup.user_id;";

/*
 * SQL Insert statements
 */
	private static final String SQL_INSERT_CONFIG = "INSERT INTO tbl_config(config_key, config_value) VALUES(?,?)";
	private static final String SQL_INSERT_USER = "INSERT INTO tbl_users(user_name, user_passwd_hash) VALUES(?,?)";
	private static final String SQL_INSERT_USER_PERMISSION = "INSERT INTO tbl_user_permissions(user_id, user_permissions) VALUES(?,?)";
	private static final String SQL_INSERT_ROLE = "INSERT INTO tbl_roles(role_name, role_permissions, role_inherits_from) VALUES(?,?,?)";
	private static final String SQL_INSERT_USER_ROLE = "INSERT INTO tbl_user_roles(user_id, role_id) VALUES(?,?)";
	private static final String SQL_GET_USER_WHERE_USERID = "SELECT * FROM view_user_roles WHERE user_id == ?";
	private static final String SQL_GET_USER_WHERE_USERNAME = "SELECT * FROM view_user_roles WHERE user_name == ?";

	private static final String SQL_GET_CONFIG="SELECT config_value FROM tbl_config WHERE config_key == ?;";
	
/*
 * Class specifics
 */
	private AuthDatabase() {
		LOGGER.info("Connecting to SQLite database...");
		dbConn = connect();
		createConfig();
		createUsers();
		createUserPermissions();
		createRoles();
	}
	
	public static AuthDatabase getInstance() {
		return instance;
	}
/*
 * Utilities
 */	
	private static Connection connect() {
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(DB_URL);
		} catch(SQLException e) {
			LOGGER.error(e.getMessage());
		}
		return conn;
	}
	
	private boolean tableExists(String tableName) {
		Integer res = 0;
		try(PreparedStatement pstmt = dbConn.prepareStatement(SQL_TBL_CHECK)) {
			pstmt.setString(1, tableName);
			ResultSet rs = pstmt.executeQuery();
			res = rs.getInt(1);
		} catch(SQLException e) {
			LOGGER.error(e.getMessage());
		}
		return (res==1);
	}

	private void executeStatement(String sql) {
		try(Statement stmt = dbConn.createStatement()) {
			stmt.execute(sql);
		} catch(SQLException se) {
			LOGGER.error(se.getMessage());
		}
	}

/*
 * User related methods
 */
	private void createUsers() {
		if(!tableExists("tbl_users")) {
			executeStatement(SQL_TBL_USERS);
			insertUser("Alice", "password");
			insertUser("Bob", "password");
			insertUser("Cecilia", "password");
			insertUser("David", "password");
			insertUser("Erica", "password");
			insertUser("Fred", "password");
			insertUser("George", "password");
		}
	}

	private void insertUser(String username, String password) {
		username = username.toLowerCase();
		String passwdHash = BCrypt.hashpw(password, BCrypt.gensalt());
		try(PreparedStatement pstmt = dbConn.prepareStatement(SQL_INSERT_USER)) {
			pstmt.setString(1, username);
			pstmt.setString(2, passwdHash);
			pstmt.executeUpdate();
		} catch (SQLException se) {
			LOGGER.error("An exception occurred.");
			se.printStackTrace();
		}
	}

	private User getUser(ResultSet rs ) throws SQLException {
		User userObj = new User(
						rs.getInt("user_id"),
						rs.getString("user_name"),
						rs.getString("user_passwd_hash"),
						new PermissionMap(rs.getString("user_permissions")),
						rs.getInt("role_id")
					);
		return userObj;
	}
	
	public User getUser(int id) {
		User userObj = null;
		try (PreparedStatement stmt = dbConn.prepareStatement(SQL_GET_USER_WHERE_USERID)) {
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			userObj = getUser(rs);
		} catch(SQLException se) {
			LOGGER.error(se.getMessage());
		}
		return userObj;
	}

	public User getUser(String username) {
		username = username.toLowerCase();
		User userObj = null;
		try (PreparedStatement stmt = dbConn.prepareStatement(SQL_GET_USER_WHERE_USERNAME)) {
			stmt.setString(1, username);
			ResultSet rs = stmt.executeQuery();
			userObj = getUser(rs);
		} catch(SQLException se) {
			LOGGER.error(se.getMessage());
		}
		return userObj;
	}

/*
 * Configuration related methods
 */
	private void createConfig() {
		if(!tableExists("tbl_config")) {
			executeStatement(SQL_TBL_CONFIG);
			try {
				SecretKey key = KeyGenerator.getInstance("AES").generateKey();
				String encodedKey = Base64.getEncoder().encodeToString(key.getEncoded());
				insertConfig("signature-key", encodedKey);
			} catch(Exception e) {
				LOGGER.error(e.getMessage());
			}
		}
	}

	private void insertConfig(String key, String value) {
		key = key.toLowerCase();
		try(PreparedStatement pstmt = dbConn.prepareStatement(SQL_INSERT_CONFIG)) {
			pstmt.setString(1, key);
			pstmt.setString(2, value);
			pstmt.executeUpdate();
		} catch (SQLException se) {
			LOGGER.error(se.getMessage());
		}
	}
	
	public String getConfig(String key) {
		key = key.toLowerCase();
		String value=null;
		try (PreparedStatement pstmt = dbConn.prepareStatement(SQL_GET_CONFIG)) {
			pstmt.setString(1, key);
			ResultSet rs = pstmt.executeQuery();
			if(!rs.next()) {
				return null;
			}
			value = rs.getString("config_value");
		} catch(SQLException e) {
			LOGGER.error(e.getMessage());
		}
		return value;
	}

	protected SecretKey getSignatureKey() {
		String encodedKey = getConfig("signature-key");
		byte[] decodedKey = Base64.getDecoder().decode(encodedKey);
		SecretKey key = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
		return key;
	}

/*
 * Permissions related things
 */
	private void createUserPermissions() {
		if(!tableExists("tbl_user_permissions")) {
			// Create tables and views;
			executeStatement(SQL_TBL_USER_PERMISSIONS);
			executeStatement(SQL_VIEW_USER_PERMISSIONS);

			// All per-user permissions maps are set to the same permissions as a User as according to the roles.
			try(Statement stmt = dbConn.createStatement()) {
				ResultSet rs = stmt.executeQuery("SELECT user_id FROM tbl_users ORDER BY user_id;");
				while(rs.next()) {
					int id = rs.getInt("user_id");
					insertUserPermission(id, "{\"com.bitbruisers.print.PrintServiceImpl\": {\"print\":true, \"queue\":true}}");
				}
			} catch(SQLException se) {
				LOGGER.error(se.getMessage());
			}
		}
	}

	private void insertUserPermission(int id, String permission) {
		try (PreparedStatement pstmt = dbConn.prepareStatement(SQL_INSERT_USER_PERMISSION)) {
			pstmt.setInt(1, id);
			pstmt.setString(2, permission);
			pstmt.execute();
		} catch(SQLException se) {
			LOGGER.error(se.getMessage());
		}
	}

/*
 * Role related things
 */
	private void createRoles() {
		if(!tableExists("tbl_roles")) {
			executeStatement(SQL_TBL_ROLES);
			insertRole("Manager", "{\"com.bitbruisers.print.PrintServiceImpl\": {\"print\":true, \"queue\":true,\"topQueue\":true,\"start\":true,\"stop\":true,\"restart\":true,\"status\":true,\"readConfig\":true,\"setConfig\":true}}",0);
			insertRole("Service Technician", "{\"com.bitbruisers.print.PrintServiceImpl\": {\"start\":true,\"stop\":true,\"restart\":true,\"status\":true,\"readConfig\":true,\"setConfig\":true}}",0);
			insertRole("User", "{\"com.bitbruisers.print.PrintServiceImpl\": {\"print\":true, \"queue\":true}}", 0);
			insertRole("Power User", "{\"com.bitbruisers.print.PrintServiceImpl\": {\"topQueue\":true, \"restart\":true}}", 3);
		}
		if(!tableExists("tbl_user_roles")) {
			executeStatement(SQL_TBL_USER_ROLES);
			executeStatement(SQL_VIEW_USER_ROLES);
			insertRoleMapping(1, 1); // Alice is a Manager
			insertRoleMapping(2, 2); // Bob is a Service Technician
			insertRoleMapping(3, 4); // Cecilia is a Power User
			insertRoleMapping(4, 3); // David is a User
			insertRoleMapping(5, 3); // Erica is a User
			insertRoleMapping(6, 3); // Fred is a User
			insertRoleMapping(7, 3); // George is a User
		}
	}

	public HashMap<Integer, Role> getRoles() {
		HashMap<Integer, Role> roles = new HashMap<Integer, Role>();
		try(Statement stmt = dbConn.createStatement()) {
			ResultSet rs = stmt.executeQuery("SELECT * FROM tbl_roles ORDER BY role_id");
			while(rs.next()) {
				// Basic role information
				int role_id = rs.getInt("role_id");
				String role_name = rs.getString("role_name");
				String role_permissions = rs.getString("role_permissions");
				int role_inherits_from = rs.getInt("role_inherits_from");
				
				/*
				 * Deal with role inheritance
				 */
				Stack<String> permission_stack = new Stack<String>();
				while(role_inherits_from != 0) {
					Statement substmt = dbConn.createStatement();
					ResultSet subrs = substmt.executeQuery("SELECT role_permissions, role_inherits_from FROM tbl_roles WHERE role_id = " + Integer.toString(role_inherits_from));
					permission_stack.push(subrs.getString("role_permissions"));
					role_inherits_from = subrs.getInt("role_inherits_from");
				}

				PermissionMap pm = new PermissionMap();
				while(!permission_stack.empty()) {
					PermissionMap tmp = new PermissionMap(permission_stack.pop());
					pm.merge(tmp);
				}

				pm.merge(new PermissionMap(role_permissions));
				roles.put(role_id, new Role(role_id, role_name, pm));
			}
		} catch(SQLException se) {
			LOGGER.error(se.getMessage());
		}
		return roles;
	}

	private void insertRole(String roleName, String permissions, int inherits_from) {
		roleName = roleName.toLowerCase();
		try(PreparedStatement pstmt = dbConn.prepareStatement(SQL_INSERT_ROLE)) {
			pstmt.setString(1, roleName);
			pstmt.setString(2, permissions);
			pstmt.setInt(3, inherits_from);
			pstmt.executeUpdate();
		} catch (SQLException se) {
			LOGGER.error("An exception occurred.");
			se.printStackTrace();
		}
	}

	private void insertRoleMapping(int user_id, int role_id) {
		try(PreparedStatement pstmt = dbConn.prepareStatement(SQL_INSERT_USER_ROLE)) {
			pstmt.setInt(1, user_id);
			pstmt.setInt(2, role_id);
			pstmt.executeUpdate();
		} catch (SQLException se) {
			LOGGER.error("An exception occurred.");
			se.printStackTrace();
		}
	}
}
