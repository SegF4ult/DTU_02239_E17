package com.bitbruisers.auth;

import java.util.Map;
import java.util.HashMap;
import com.bitbruisers.auth.models.User;
import com.bitbruisers.auth.AuthToken;
import com.bitbruisers.auth.AuthTokenInvalidException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import javax.crypto.SecretKey;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class AuthTokenHandler {
	private static final Logger LOGGER = LogManager.getRootLogger();
	private static final AuthTokenHandler instance = new AuthTokenHandler();
	private static final String ISSUER = "AuthPrintSrv";
	private SecretKey signatureKey = null;

	private AuthTokenHandler() {
		LOGGER.info("Instantiating AuthTokenHandler...");
	}

	public void setSignatureKey(SecretKey newKey) {
		this.signatureKey = newKey;
	}

	public boolean hasAuthTokenExpired(AuthToken tkn) {
			String jws = tkn.getJws();
			boolean isExpired = false;

			try {
				Date expires = Jwts.parser().setSigningKey(signatureKey).parseClaimsJws(jws).getBody().getExpiration();
				Date current = Date.from(LocalDateTime.now().toInstant(ZoneOffset.UTC));
				isExpired = (current.compareTo(expires) > 0);
			} catch(Exception e) {
				LOGGER.error(e.getMessage());
			}
			return isExpired;
	}

	public boolean isAuthTokenVerified(AuthToken tkn) throws AuthTokenInvalidException {
		String jws = tkn.getJws();
		boolean issuerEqual = false;
		boolean hasExpired = hasAuthTokenExpired(tkn);

		try {
			issuerEqual = Jwts.parser().setSigningKey(signatureKey).parseClaimsJws(jws).getBody().getIssuer().equals(ISSUER);
		} catch(SignatureException se) {
			LOGGER.error(se.getMessage());
			throw new AuthTokenInvalidException("Authentication Token is invalid.");
		}
		return (issuerEqual && !hasExpired);
	}

	public AuthToken buildAuthToken(User usr) {
		Map<String, Object> map = new HashMap<>();
		map.put("uid", usr.getId());
		LocalDateTime ldtIssued = LocalDateTime.now();
		LocalDateTime ldtExpires = ldtIssued.plusMinutes(30);
		Date dtIssued = Date.from(ldtIssued.toInstant(ZoneOffset.UTC));
		Date dtExpires = Date.from(ldtExpires.toInstant(ZoneOffset.UTC));
		String jws = Jwts.builder()
				.setClaims(map)
				.setIssuer(ISSUER)
				.setSubject(usr.getUsername())
				.setIssuedAt(dtIssued)
				.setExpiration(dtExpires)
				.signWith(SignatureAlgorithm.HS512, signatureKey)
				.compact();
		AuthToken tkn = new AuthTokenImpl(jws);
		return tkn;
	}

	public AuthToken buildAuthToken(String username, int userID) {
		Map<String, Object> map = new HashMap<>();
		map.put("uid", userID);
		LocalDateTime ldtIssued = LocalDateTime.now();
		LocalDateTime ldtExpires = ldtIssued.plusMinutes(30);
		Date dtIssued = Date.from(ldtIssued.toInstant(ZoneOffset.UTC));
		Date dtExpires = Date.from(ldtExpires.toInstant(ZoneOffset.UTC));
		String jws = Jwts.builder()
				.setClaims(map)
				.setIssuer(ISSUER)
				.setSubject(username)
				.setIssuedAt(dtIssued)
				.setExpiration(dtExpires)
				.signWith(SignatureAlgorithm.HS512, signatureKey)
				.compact();
			
		AuthToken tkn = new AuthTokenImpl(jws);
		return tkn;
	}

	public static AuthTokenHandler getInstance() {
		return instance;
	}
}
