package com.bitbruisers.auth.models;

import com.bitbruisers.auth.models.PermissionMap;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Role {
	private static final Logger LOGGER = LogManager.getRootLogger();
	private int id = 0;
	private String name = null;
	private PermissionMap permissions = null;

	public Role(int id, String name, PermissionMap permissions) {
		this.id = id;
		this.name = name;
		if(permissions == null)
			LOGGER.error("PermissionMap passed is null...");
		this.permissions = permissions;
	}

	public int getId() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}

	public PermissionMap getPermissions() {
		return this.permissions;
	}

	@Override
	public String toString() {
		return "["+Integer.toString(this.id)+"] " + this.name + " " + this.permissions;
	}
}
