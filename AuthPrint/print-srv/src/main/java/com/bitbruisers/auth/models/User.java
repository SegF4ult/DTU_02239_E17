package com.bitbruisers.auth.models;

import com.bitbruisers.auth.models.PermissionMap;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class User {
	private static final Logger LOGGER = LogManager.getRootLogger();
	private int id = 0;

	private String username = null;

	private String password_hash = null;

	private PermissionMap permissions = null;

	private int role_id = 0;

	public User(int id, String username, String password_hash) {
		this.id = id;
		this.username = username;
		this.password_hash = password_hash;
	}

	public User(int id, String username, String password_hash, PermissionMap permissions) {
		this.id = id;
		this.username = username;
		this.password_hash = password_hash;
		if(permissions == null) {
			LOGGER.error("PERMISSIONS ARE NULL!");
		}
		this.permissions = permissions;
	}

	public User(int id, String username, String password_hash, PermissionMap permissions, int role_id) {
		this.id = id;
		this.username = username;
		this.password_hash = password_hash;
		if(permissions == null) {
			LOGGER.error("PERMISSIONS ARE NULL!");
		}
		this.permissions = permissions;
		this.role_id = role_id;
	}

	public int getId() {
		return this.id;
	}

	public String getUsername() {
		return this.username;
	}

	public String getPasswordHash() {
		return this.password_hash;
	}

	public PermissionMap getPermissions() {
		return this.permissions;
	}

	public int getRoleId() {
		return this.role_id;
	}

	@Override
	public String toString() {
		return "["+Integer.toString(this.id)+"] " + this.username + " " + this.password_hash + " " + this.permissions;
	}
}
