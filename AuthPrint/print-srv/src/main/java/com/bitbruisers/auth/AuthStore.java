package com.bitbruisers.auth;

import com.bitbruisers.auth.models.User;
import com.bitbruisers.auth.models.Role;
import com.bitbruisers.auth.AuthDatabase;
import com.bitbruisers.auth.AuthException;
import com.bitbruisers.auth.AuthService;
import com.bitbruisers.auth.AuthToken;
import com.bitbruisers.auth.AuthTokenHandler;
import com.bitbruisers.auth.AuthTokenInvalidException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import javax.crypto.SecretKey;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.mindrot.jbcrypt.BCrypt;
import java.util.HashMap;

public class AuthStore {
	private static final Logger LOGGER = LogManager.getRootLogger();
	private static final AuthStore instance = new AuthStore();
	private AuthDatabase db = null;
	private AuthTokenHandler ath = null;
	private SecretKey signatureKey = null;
	private HashMap<Integer, Role> rolesById = null;

	private AuthStore() {
		LOGGER.info("Instantiating Authentication Store...");
		db = AuthDatabase.getInstance();
		ath = AuthTokenHandler.getInstance();
		signatureKey = db.getSignatureKey();
		ath.setSignatureKey(signatureKey);
		rolesById = db.getRoles();
	}

	public boolean hasAccess(AuthToken tkn) throws AuthTokenInvalidException {
		User currentUser = null;
		if(ath.isAuthTokenVerified(tkn)) {
			currentUser = db.getUser(tkn.getUserId());
		}

		if(currentUser == null) {
			return false;
		}

		if(currentUser.getPermissions() == null && currentUser.getRoleId() == 0) {
			return false;
		}
		
		String callerClass = Thread.currentThread().getStackTrace()[2].getClassName();
		String callerMethod = Thread.currentThread().getStackTrace()[2].getMethodName();

		// If the user does not have an associated role, fall back onto its basic user permissions
		if(currentUser.getRoleId() == 0) {
			return currentUser.getPermissions().hasPermission(callerClass, callerMethod);
		}
		return rolesById.get(currentUser.getRoleId()).getPermissions().hasPermission(callerClass, callerMethod);
	}

	public AuthToken authenticate(String username, String passwd) throws AuthException {
		User user = db.getUser(username);
		
		boolean authenticated = false;
		if(user != null && BCrypt.checkpw(passwd, user.getPasswordHash())) {
			authenticated = true;
		}

		if(!authenticated) {
			throw new AuthException("Username and/or password incorrect.");
		}

		AuthToken tkn = ath.buildAuthToken(user);
		return tkn;
	}

	public static AuthStore getInstance() {
		return instance;
	}
}
