package com.bitbruisers.auth;

import com.bitbruisers.auth.AuthException;
import com.bitbruisers.auth.AuthService;
import com.bitbruisers.auth.AuthToken;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class AuthServiceImpl extends UnicastRemoteObject implements AuthService {
	private static final Logger LOGGER = LogManager.getRootLogger();
	private AuthStore as = null;

	public AuthServiceImpl() throws RemoteException {
		super();
		as = AuthStore.getInstance();
	}

	public AuthToken authenticate(String user, String pass) throws AuthException, RemoteException {
		return as.authenticate(user, pass);
	}
}
