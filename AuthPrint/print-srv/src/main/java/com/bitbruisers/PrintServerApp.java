package com.bitbruisers;

import com.bitbruisers.auth.AuthServiceImpl;
import com.bitbruisers.print.PrintServiceImpl;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import javax.rmi.ssl.SslRMIClientSocketFactory;
import javax.rmi.ssl.SslRMIServerSocketFactory;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class PrintServerApp {
	private static final int RMI_PORT = 42000;
	private static final Logger LOGGER = LogManager.getRootLogger();
	private static final String KEYSTORE_FILE = "server.keystore";
	private static final String KEYSTORE_PASS = "storepass";

	public static void main(String[] args) {
		LOGGER.info("Setting up application keystore...");
		System.setProperty("javax.net.ssl.keyStore", KEYSTORE_FILE);
		System.setProperty("javax.net.ssl.keyStorePassword", KEYSTORE_PASS);

		LOGGER.info("Setting up secure socket factories...");
		SslRMIClientSocketFactory csf = new SslRMIClientSocketFactory();
		SslRMIServerSocketFactory ssf = new SslRMIServerSocketFactory();		
		
		LOGGER.info("Initialization complete...");
		try {
			LOGGER.info("Creating RMI registry at port " + Integer.toString(RMI_PORT) + "...");
			Registry reg = LocateRegistry.createRegistry(RMI_PORT, csf, ssf);
			LOGGER.info("Binding authentication interface...");
			reg.rebind("auth", new AuthServiceImpl());
			LOGGER.info("Binding printing interface...");
			reg.rebind("print", new PrintServiceImpl());
		} catch(Exception e) {
			System.err.println("[Server] An exception has occurred: ");
			e.printStackTrace();
		}
	}
}
