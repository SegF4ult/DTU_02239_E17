package com.bitbruisers.print;

import com.bitbruisers.auth.AuthStore;
import com.bitbruisers.auth.AuthToken;
import com.bitbruisers.auth.AuthTokenInvalidException;
import com.bitbruisers.print.PrintService;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class PrintServiceImpl extends UnicastRemoteObject implements PrintService {
		private static final Logger LOGGER = LogManager.getRootLogger();
		private static final AuthStore as = AuthStore.getInstance();

		public PrintServiceImpl() throws RemoteException {
				super();
		}

		public void print(AuthToken tkn,String filename, String printer) throws AuthTokenInvalidException, RemoteException {
				if(as.hasAccess(tkn))
						LOGGER.info(filename + " " + printer);
		}

		public void queue(AuthToken tkn) throws AuthTokenInvalidException, RemoteException {
				if(as.hasAccess(tkn))
						LOGGER.info("queue()");
		}

		public void topQueue(AuthToken tkn,int job) throws AuthTokenInvalidException, RemoteException {
				if(as.hasAccess(tkn))
						LOGGER.info("topQueue() " + Integer.toString(job));
		}

		public void start(AuthToken tkn) throws AuthTokenInvalidException, RemoteException {
				if(as.hasAccess(tkn))
						LOGGER.info("start()");
		}

		public void stop(AuthToken tkn) throws AuthTokenInvalidException, RemoteException {
				if(as.hasAccess(tkn))
						LOGGER.info("stop()");
		}

		public void restart(AuthToken tkn) throws AuthTokenInvalidException, RemoteException {
				if(as.hasAccess(tkn))
						LOGGER.info("restart()");
		}

		public void status(AuthToken tkn) throws AuthTokenInvalidException, RemoteException {
				if(as.hasAccess(tkn))
						LOGGER.info("status()");
		}

		public void readConfig(AuthToken tkn,String parameter) throws AuthTokenInvalidException, RemoteException {
				if(as.hasAccess(tkn))
						LOGGER.info(parameter + " : <value>");
		}

		public void setConfig(AuthToken tkn,String parameter, String value) throws AuthTokenInvalidException, RemoteException {
				if(as.hasAccess(tkn))
						LOGGER.info(parameter + " = " + value);
		}
}

