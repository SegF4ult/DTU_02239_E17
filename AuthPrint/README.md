Author: Xander Gregory Bos <xander.g.bos@gmail.com, s170888@student.dtu.dk>

This project was built in Java, using Gradle for a build system.
To build the software and generate the necessary keystores, run the following commands:
	
	gradle build	
	gradle keystores

To run the server, run the following:

	gradle :print-srv:run

To run the client, run the following:

	gradle :print-cli:run

It binds two separated RMI interfaces at localhost:42000.
One of these interfaces is purely used for Authentication,
while the other makes use of AuthTokens to prove to the server that we are authenticated.

The standard expiration time for these tokens is 30 minutes.
