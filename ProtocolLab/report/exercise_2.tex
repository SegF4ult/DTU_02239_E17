\section*{Exercise 2: AMP}

\subsection*{Protocol Description}
The initial knowledge makes it quite clear that $A$ knows all the parties involved, where-as $B$ only knows $s$ and itself, and $s$ only knows $A$ and itself. This knowledge comes with the relevant asymmetric keys being exchanged beforehand.
The protocol tries to establish a trust between $B$ and $A$ by use of a trusted third party $s$.

The protocol starts with $A$ requesting knowledge of $B$. $B$, not knowing who $A$ is, tells $A$ that he will consider, provided $s$ vouch for him. Next, $A$ approaches $s$ and would like his request signed by $s$. This trusted party obliges and gives $A$ a ticket to present to $B$, with a signature on it.
At this stage, $A$ sends the ticket to $B$ and receives the original $Request$ and the appropriate $Data$.

\subsection*{Protocol Analysis}
Running the AMP protocol through OFMC yields a weak authentication violation. The attacktrace is visualized in Figure~\ref{fig:amp_attack}.

\begin{figure}[htb]
\centering
\includegraphics[width=.85\textwidth]{AMP_attacktrace.png}
\caption{Attacktrace for the AMP protocol}
\label{fig:amp_attack}
\end{figure}

At the start of the protocol, $x802$ in the role of $A$, sends a request to the intruder, who is then in the role of $B$. The intruder takes the information from the request, substitutes his name (re-uses the same key) and poses in the role of $A$, sending a request to $x801$, who believes to be in the role of $B$.

From there, the intruder forwards $x801$'s response to $x802$ and intercepts the message from $x802$. The intruder forwards that message to $s$, who believes the intruder is assuming the role of $A$. When the elaborate handshaking between all the parties is done, the intruder then intercepts the $Data$ and concludes his work. Because of the dishonesty of the intruder, $x802$ and $x801$ believe to be communicating with other people than they are actually communicating with. This is why the authentication goals cannot be met by the current description of the protocol.

\subsection*{Fixing the AMP protocol}
The weak authentication that is reported by OFMC is consistent with the problem of role reversal and the relevant confusion caused by it. For instance, $B$ thinks that he's actually performing the role of $A$. By including both $A$ and $B$ in the responses, this weak authentication issue is fixed. The fix is written out in Listing~\ref{lst:amp_weak_fix}.

\begin{lstlisting}[caption=Weak authentication fixed, label=lst:amp_weak_fix]
...
# s now includes both A and B in the ticket it gives back to A
s -> A: { {A, B, s}inv(pk(s)) }pk(A)
# A can now send this to B, encrypted under the session key K
A -> B: {| {A, B, s}inv(pk(s)) |}K
...
\end{lstlisting}

Re-analyzing the protocol now leads to a strong authentication violation (Listing~\ref{lst:amp_strong_auth}). Indicated by the session numbers, now there are possibilities for replay attacks. To fix this, we can add nonces and timestamps to the various messages, in order to ensure freshness.
\begin{lstlisting}[language={},caption=Strong authentication attack trace, label=lst:amp_strong_auth]
ATTACK TRACE:
(x1002,1) -> i: {x1002,x1001,Request(1),K(1)}_(pk(x1001))
i -> (x1001,1): {x1002,x1001,Request(1),K(1)}_(pk(x1001))
(x1001,1) -> i: {|x1002,s,x1001,ReqID(2),Request(1)|}_K(1)
i -> (x1001,2): {x1002,x1001,Request(1),K(1)}_(pk(x1001))
(x1001,2) -> i: {|x1002,s,x1001,ReqID(3),Request(1)|}_K(1)
i -> (x1002,1): {|x1002,s,x1001,ReqID(3),Request(1)|}_K(1)
(x1002,1) -> i: {{x1002,s,x1001,ReqID(3),Request(1)}_inv(pk(x1002))}_(pk(s))
i -> (s,1): {{x1002,s,x1001,ReqID(3),Request(1)}_inv(pk(x1002))}_(pk(s))
(s,1) -> i: {{x1002,x1001,s}_inv(pk(s))}_(pk(x1002))
i -> (x1002,1): {{x1002,x1001,s}_inv(pk(s))}_(pk(x1002))
(x1002,1) -> i: {|{x1002,x1001,s}_inv(pk(s))|}_K(1)
i -> (x1001,1): {|{x1002,x1001,s}_inv(pk(s))|}_K(1)
(x1001,1) -> i: {|Request(1),Data(7)|}_K(1)
i -> (x1001,2): {|{x1002,x1001,s}_inv(pk(s))|}_K(1)
(x1001,2) -> i: {|Request(1),Data(8)|}_K(1)
\end{lstlisting}
\newpage

After adding the nonces and timestamps, OFMC finds no further attacks. A formal description, including all of the fixes mentioned above can be found in Listing~\ref{lst:amp-fixed}.

\begin{lstlisting}[caption=AMP with fixes,label=lst:amp-fixed]
Protocol: AMP_fixed
Types:	Agent A, s, B;
	Number Request, ReqID, Data;
	Number NA, NB, T0; # Nonces and timestamps
	Symmetric_key K
Knowledge: 
	A: A, s, B, pk(s), pk(A), inv(pk(A)), pk(B);
	s: A, s, pk(s), inv(pk(s)), pk(A);
	B: s, B, pk(s), pk(B), inv(pk(B))
	where B!=s
Actions:

A->B: {A, B, Request, K}pk(B)
# Nonce NB and Timestamp T0 added 
B->A: {|A, B, s, T0, ReqID, Request, NB|}K

# A adds his own nonce 
A->s: { {A, B, s, T0, ReqID, Request }inv(pk(A)), NA }pk(s)
# s dutifully sends NA back
# The ticket includes all parties, to prevent confusion
s->A: { {A, B, s}inv(pk(s)), NA }pk(A)

# A sends the ticket along with Nonce NB 
A->B: {|{A, B, s}inv(pk(s)), NB|}K
B->A: {| Request, Data|}K

Goals:
B authenticates A on Request
A authenticates B on Data
Data secret between B, A
\end{lstlisting}

\subsection*{Dishonest third party}
If we replace $s$ by $S$, the protocol breaks down. The entirety of the protocol is based around $s$ being a trusted third party. By allowing $S$ to be either honest or dishonest, the intruder can take on the role and falsely vouch for any party. The intruder can now forge tickets for the agent in the role of $A$ to pass on to $B$. Since nobody verifies the authentication of $S$, anyone can be verifying these authentication requests.

By implying that we need to have $S$ properly authenticated in order to trust him, we're implicitly adding a new goal to our current set of goals. This new goal in turn warrants the addition of new initial knowledge. There is no way, given the current goals and initial knowledge that $S$ can ever be a trustworthy participant in this protocol.