\section*{Exercise 1: Kerberos PKInit}

\subsection*{Protocol Description}

\subsubsection*{Key Knowledge}
At the start of the protocol, client $C$ has knowledge of all the various actors: $ath$, $g$ and $s$. Initially $C$ also knows the following asymmetric keys.
\begin{itemize}
    \item $C$'s private key: $inv(pk(C))$
    \item $C$'s public key: $pk(C)$
    \item $ath$'s public key: $pk(ath)$
\end{itemize}

Over the course of the protocol, $C$ learns of the following symmetric keys.
\begin{itemize}
    \item A temporary key: $Ktemp$
    \item The key shared between $C$ and $g$: $KCG$
    \item The key shared between $C$ and $s$: $KCS$
\end{itemize}

\subsubsection*{Message Decryption}
With the knowledge of various keys gained during the protocol, $C$ can decrypt the following messages.
\begin{itemize}
    \item $\{T0,N1,hash(C,g,N1)\}inv(pk(C))$
    \item $\{tag,{Ktemp}inv(pk(ath))\}pk(C)$
    \item $\{|g,KCG,T1,N1|\}Ktemp$
    \item $\{|C,T1|\}KCG$
    \item $\{|s,KCS,T2,N2|\}KCG$
    \item $\{|C,hash(T2)|\}KCS$
    \item $\{|hash(T2)|\}KCS$
    \item $\{|tag,Payload|\}KCS$
\end{itemize}
\newpage

\subsubsection*{Illegitimate Access}
This variation of the Kerberos protocol makes use of Public-Key cryptography for authentication, instead of relying on passwords. The initial request is signed with the client's private key and a timestamp is attached. Nobody (assuming proper key management) should be able to forge a request with another user's private key.

The signature makes it so that only the holder of the private key in question could have formulated the request. No third party could forge that request or replay a request, due to the fact that it is signed and timestamped.

\subsection*{Protocol Vulnerability}
The Kerberos PKInit protocol description is vulnerable to a Man-in-the-Middle attack. An intruder is able to intercept $Cli$~'s~\footnote{$Cli$ = $C$} request and formulate its own request to the authentication server. Then, the intruder receives a reply from the authentication server and hands the proper information over to $Cli$. Now, the intruder can listen in on communications between $Cli$ and the intended server.


This attack is possible to due to the fact that $Cli$ and $auth$~\footnote{$auth$ = $ath$} can't authenticate each other on a shared secret. A fixed description is provided in Listing~\ref{lst:pkinit_fixed}. A new number has been introduced, which is encrypted with $auth$'s public key in the request. In the reply, the same number is encrypted with $Cli$'s public key and put alongside $K_{tmp}$. In this way, the secret remains a secret between $Cli$ and $auth$ and by inclusion of $K_{tmp}$, $auth$ can be authenticated on $K_{tmp}$.\newpage

\begin{lstlisting}[caption=PKInit protocol with fixes,label=lst:pkinit_fixed]
Protocol: Kerberos_PKINIT_fixed_setup
Types:	Agent auth, Cli, grp;
	Number N1, N2, T0, T1, T2;	# Nonces and timestamps
	Number Secr, Payload, tag;	# Payload, secret etc.
	Function pk, hash, sk;
	Symmetric_key KCG, Ktmp, kag;
Knowledge:
	auth:	auth, Cli, grp, hash, pk, tag, 
		pk(auth), pk(Cli), inv(pk(auth)), kag;
	Cli:	auth, Cli, grp, hash, pk, tag, 
		pk(auth), pk(Cli), inv(pk(Cli));
where Cli!=auth

Actions:
# Added {Secr}pk(auth) to the message and its digest.
Cli -> auth: Cli, grp, N1, {Secr}pk(auth), 
	{T0, N1, hash(Cli, grp, N1, {Secr}pk(auth))}inv(pk(Cli))
# Added {Secr}pk(Cli), so Secr remains secret between Cli and auth.
auth -> Cli: Cli, 
	({|auth, Cli, grp, KCG, T1|}kag), 
	({|grp, KCG, T1, N1|}Ktmp), 
	{tag, {{Secr}pk(Cli), Ktmp}inv(pk(auth))}pk(Cli)

Goals:
Cli authenticates auth on Ktmp
Ktmp secret between auth, Cli
KCG secret between auth, Cli
\end{lstlisting}

\subsection*{Distributed Key Generation}
In the previous part of the assignment, one of the goals was to authenticate the authentication server on $K_{tmp}$, a key generated without $Cli$ having prior knowledge of it. This provides a possible attack vector, because $Cli$ cannot authenticate $auth$ on $K_{tmp}$ without more proof of $auth$~'s true identity.
Suggested is the inclusion of distributed key generation by all involved parties: $auth$ and $Cli$. This way, $Cli$ can authenticate $auth$ on the secret key they generated together. A formal OFMC description of this can be found in Listing~\ref{lst:pkinit_dh}.\newpage

\begin{lstlisting}[caption=PKInit protocol with distributed key generation,label=lst:pkinit_dh]
Protocol: Kerberos_PKINIT_DH_setup
Types:	Agent auth, Cli, grp;
	Number N1, N2, T0, T1;	# Nonces and timestamps
	Number gen, DHX, DHY;	# DH key exchange
	Number tag, Payload;	# Payload etc.
	Function pk, hash, sk;
	Symmetric_key KCG, kag;
Knowledge:
	auth:	auth, Cli, grp, gen, hash, tag, pk, pk(auth), pk(Cli), inv(pk(auth)), kag;
	Cli:	auth, Cli, grp, gen, hash, tag, pk, pk(auth), pk(Cli), inv(pk(Cli));
where Cli!=auth

Actions:
# Client requests auth for grp
Cli -> auth: Cli, grp, N1, 
	{T0, N1, hash(Cli, grp, N1)}inv(pk(Cli))
# Auth initialies DH key exchange
auth -> Cli: {auth, Cli, exp(gen, DHX)}inv(pk(auth))
# Cli responds with own part of the key
Cli -> auth: {auth, Cli, exp(gen, DHY)}inv(pk(Cli))
# Auth proceeds to return the ticket
# along with necessary keys to Cli
auth -> Cli: Cli, 
	({|auth, Cli, grp, KCG, T1|}kag), 
	({|tag, grp, KCG, T1, N1|}exp(exp(gen, DHX), DHY))

Goals:
Cli authenticates auth on exp(exp(gen, DHX), DHY)
exp(exp(gen, DHX), DHY) secret between auth, Cli
KCG secret between auth, Cli
\end{lstlisting}
\newpage